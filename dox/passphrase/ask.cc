#include "passphrase.ih"

void Passphrase::ask(string const &prompt)
{
    Tty tty;
    tty.echo(Tty::OFF);

    ifstream in;
    Exception::open(in, "/dev/tty");

    cout << prompt << ": " << flush;
    getline(in, d_pp);

    tty.echo(Tty::ON);

    cout << endl;
}
