#define XERR
#include "passphrase.ih"

string const &Passphrase::extend()
{
    string head = d_pp;

    while (d_pp.length() < s_minimumLength)
        d_pp += head;

    return d_pp;
}
