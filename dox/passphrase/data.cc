#include "passphrase.ih"

char const Passphrase::s_cryptMethod[] = "AES-256-GCM";

size_t Passphrase::s_acceptedLength = 10;

size_t Passphrase::s_minimumLength = 
                ISymCryptStream<DECRYPT>::keyLength(s_cryptMethod);

