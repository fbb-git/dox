#define XERR
#include "passphrase.ih"

string const &Passphrase::confirm(string const &mid)
{
    while (true)
    {
        ask("Enter " + mid + "passphrase");

        if (d_pp.length() < s_acceptedLength)
        {
            cout << "Passphrase must be at least " << s_acceptedLength <<
                    " characters long. Try again\n";
            continue;
        }
        string pp2 = d_pp;
        ask("Enter " + mid + "passphrase again");

        if (d_pp == pp2)                // identical passphrases
            return extend();            // ensure required encryption length

        cout << "Different passphrases. Try again.\n";
    }
}
