#ifndef INCLUDED_PASSPHRASE_
#define INCLUDED_PASSPHRASE_

#include <string>

class Passphrase
{
    std::string d_pp;

    static size_t s_minimumLength;
    static size_t s_acceptedLength;
    static char const s_cryptMethod[];

    public:
        std::string const &request();
        std::string const &confirm(std::string const &mid = "");

        std::string const &passphrase() const;          // inline

        static char const *cryptMethod();

    private:
        std::string const &extend();
        void ask(std::string const &prompt);
};

inline std::string const &Passphrase::passphrase() const
{
    return d_pp;
}

// static
inline char const *Passphrase::cryptMethod()
{
    return s_cryptMethod;
}
        
#endif
