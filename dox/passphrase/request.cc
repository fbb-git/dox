#define XERR
#include "passphrase.ih"

string const &Passphrase::request()
{
    ask("Enter passphrase");
    return extend();
}

