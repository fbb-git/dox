#define XERR
#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"add",          'a'},
        Arg::LongOption{"define",       'd'},
        Arg::LongOption{"help",         'h'},
        Arg::LongOption{"insert",       'i'},
        Arg::LongOption{"list",         'l'},
        Arg::LongOption{"List",         'L'},       // also shows passwords
        Arg::LongOption{"passphrase",   'p'},
        Arg::LongOption{"terminate",    't'},
        Arg::LongOption{"version",      'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("ad:hilLptv", longOpts, longEnd, 
                                                   argc, argv);
    arg.versionHelp(usage, Icmake::version, 0);

    DoX dox;

    return dox.run();
}
catch (...)
{
    return handleException();
}





