NOUSERMACRO(DoX)

includefile(../../release.yo)

htmlbodyopt(text)(#27408B)
htmlbodyopt(bgcolor)(#FFFAF0)
whenhtml(mailto(Frank B. Brokken: f.b.brokken@rug.nl))

DEFINEMACRO(lsoption)(3)(\
    bf(--ARG1)=tt(ARG3) (bf(-ARG2))\
)
DEFINEMACRO(laoption)(2)(\
    bf(--ARG1)=tt(ARG2)\
)
DEFINEMACRO(loption)(1)(\
    bf(--ARG1)\
)
DEFINEMACRO(soption)(1)(\
    bf(-ARG1)\
)


DELETEMACRO(tt)
DEFINEMACRO(tt)(1)(em(ARG1))

COMMENT( man-request, section, date, distribution file, general name)
manpage(DoX)(1)(_CurYrs_)(DoX._CurVers_)
        (DoX - X-Window command inserter)

COMMENT( man-request, larger title )
manpagename(DoX)(X-window system window commmand inserter)

manpagesynopsis()
       bf(dox) [OPTIONS] [tt(database)]

manpagedescription()

    The program bf(dox) is used to insert commands, possibly requiring
passwords/passphrases in selected windows. 

When starting bf(dox) the first time it requires one argument: the location of
bf(dox's) database. The database must also be specified with several options
(cf. section bf(OPTIONS) below). When no database and no options are specified
bf(dox) inserts a command in a selected window.

The database itself is encrypted (using the tt(AES-256-GCM) symmetric
encryption algorithm). The passphrase used by the encryption algorithm
is specified by the user and must at least contain 10 characters.

When bf(dox) is started for the first time it starts a daemon process, which
is thereafter contacted by the tt(DoX) client program. The client program can
be started as a command or it can be started using a keybinding
(cf. bf(bash)(1)'s tt(bind) command or bf(tcsh)(1)'s tt(bindkey)
command). E.g, using bf(tcsh)(1): after adding
        verb(
    bindkey -c "^x" '~/bin/dox'
        )
    to tt(.tchrc) pressing tt(ctrl-x) in a terminal window starts
bf(dox). Likewise, using bf(bash)(1), a command like
        verb(
    bind -x '"\C-x":"~/bin/dox"'
        )
    can be defined in, e.g., tt(.bashrc).

bf(Dox) then requests you to click your mouse in a window of your
choice. If that window's name is defined in bf(xdo)'s database the command
defined for that window is executed in that window. 
manpagesection(RETURN VALUES)

    tt(DoX) returns 0 to when the requested command could be inserted into the
indicated window. Otherwise 1 is returned.

manpageoptions()

    When defined, single letter options are listed between parentheses
following their associated long-option variants. Single letter options require
arguments if their associated long options require arguments as well.

    itemization(
    it() loption(add) soption(a)nl()
       Interactively add a new entry to the database. It requires that the
        daemon isn't currently active, and the name of the current (or new)
        database must be specified as bf(dox's) argument. nl()
       Interactively the name of the window is requested. The specified window
        doesn't have to be open when using tt(--add), but the label shown when
        the window em(is) open must be identical to the specified window
        name. nl()
       Next the command to issue is requested. First the question 
       verb(
    Show the entered command? [yN] 
       )
       is shown. When the tt(Enter) key is pressed the command is not
        echoed. It em(is) echoed when tt(y) followed by tt(Enter) is
        pressed. The command must consist of one line, using the following
        format: 
    quote(
        itemization(
        it() command characters themselves are entered as plain
            ascii-characters;
        it() use tt(\n) (note: not tt(\\n)) indicates the tt(Enter) key;
        it() between tt(\{) and tt(\}) a delay in milli-seconds is specified
            (which is commonly used after issuing a command to start a
            program, to allow the program to start);
        it() a passphrase is surrounded by tt(\[) and tt(\]). Using the
            tt(--list) option (see below) passphrases are not
            shown. Using the tt(--List) option passphrases em(are) shown.
        )

       Example:
       verb(
    crypt -c\n\{1000\}\[a seCret paSsphrasE!\]\n
       )
    )

    it() lsoption(define)(d)(datatxt)nl() 
       To define a database from scratch the tt(--define) option is used. The
        tt(datatxt) file is a plain text file using pairs of lines. The first
        line of each pair defined the window name, the second line defines the
        command to issue. The pathname of the (not yet existing) database must
        be specified as bf(dox's) argument. nl()
       

    it() loption(help) (soption(h))nl()
       Basic usage information is written to the standard output stream,
        whereafter tt(DoX) terminates.

    it() loption(list) soption(l)nl()
       The pathname of the database must be specified as bf(dox's)
        argument. nl() 
        The entries of the database are written to the standard output
        stream. Passphrases are not shown, but an ellipses (tt(...)) are
        displayed instead.

    it() loption(List) soption(L)nl()
       Same as option tt(--list), but also showing the passphrases;

    it() loption(terminate) soption(t)nl() 
       The daemon is terminated (whereafter tt(DoX) itself also terminates);

    it() loption(version) (soption(v))nl()
       tt(DoX)'s version number is written to the standard error stream
        whereafter tt(DoX) terminates.
    )


manpagesection(SEE ALSO)
    bf(bash)(1), bf(keepass2)(1), bf(tcsh)(1)

manpagebugs()

    None reported

manpagesection(ABOUT DoX)

    After using the tt(keepass) proram for years, it recently was upgraded to
bf(keepass2). Unfortunately the auto-key typing facility couldn't be made to
work when using tt(keepass2). After an extensive exchange of ideas and options
on the sourceforge url(keepass discussion list)
(https://sourceforge.net/p/keepass/discussion/329220/thread/f15acdbdb9/) I
gave in and decided to develop an alternative for tt(keepass2). I still use
tt(keepass2) as a backup for username and password combinations, but not
anymore for command-entry in specific windows. 


manpageauthor()

Frank B. Brokken (f.b.brokken@rug.nl).
