//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_( [options] [arg]
Where:
    [options] - optional arguments (short options between parentheses):
        --add (-a)          - add a new entry to 'arg'
        --define (-d) data  - define the db from the data txt file.
                              'arg' required but may not exist 
        --help (-h)         - provide this help
        --list (-l)         - show the entries in the 'arg' database, not
                              showing their passphrases
        --List (-L)         - show the entries in 'arg' including their
                              passphrases
        --passphrase (-p)   - change the current passphrase of 'arg'
        --terminate (-t)    - terminate: end the daemon
        --version (-v)      - show version information and terminate
    
    arg:
        [none]          - insert the command in the window receiving a mouse 
                          click
        path/to/dox.db  - required with options --list, --List, and
                            --passphrase.
                          when no options are specified: 
                            start the daemon, load the command db, and open
                            /home/frank/etc/dox.uds 
                          when option --insert is also specified the command
                          assiciated with the window where the daemon is
                          started is inserted.

    communication with the daemon: via the /home/frank/etc/dox.uds 
    Unix Domain Socket.

)_";

//  doxtool getwindowname `doxtool getactivewindow`
//  doxtool getwindowname `doxtool selectwindow`
//  doxtool key --window 27262987 KP_Enter
//  doxtool type --window 27262987 text to enter into the window
//  BackSpace, not Backspace

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmake::author << "\n" <<
    progname << " V" << Icmake::version << " " << Icmake::years << "\n"
    "\n"
    "Usage: " << progname << info;
}





