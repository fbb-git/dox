#define XERR
#include "dox.ih"

void DoX::addDb()
{
    noUds("end the daemon before adding entries to the database");

    verifyPassphrase();                 // opens d_decrypt

    string windowName = newWindow();

    UPtrEncrypt encrypt = copyDb(windowName);

    string cmd = newCmd();

    if (cmd.empty())
    {
        fs::remove(d_newDbName, s_errorCode);
        throw Exception{} << "No command specified for window " << windowName;
    }

    *encrypt << windowName << '\n' << cmd << '\n';

    endCopy(encrypt);
}





