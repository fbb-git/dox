#define XERR
#include "dox.ih"

int DoX::forward()
{
    CoutExtractor extractor;
    extractor.execute("/usr/bin/xdotool getactivewindow");

    string windowID;
    getline(extractor, windowID);

    LocalClientSocket uds{ s_uds };

    int fd = connect(uds);

    OFdStream out{ fd };
    IFdStream in{ fd };                   // stream to read from the daemon
                                         // received by the daemon
    out << d_xDoTool.windowName() << '\n' <<
           (windowID == d_xDoTool.windowID() ? '-' : ' ') <<
           d_xDoTool.windowID() << endl;

    return waitForReply(in);
}
