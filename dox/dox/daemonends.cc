#define XERR
#include "dox.ih"

void DoX::daemonEnds()
{
    LocalClientSocket uds{ s_uds };

    int fd = connect(uds);

    OFdStream out{ fd };
    IFdStream in(fd);                   // stream to read from the daemon

    out << "quit" << endl;              // send quit to the daemon

    waitForReply(in);
}
