#include "dox.ih"

// overrides
DoX::~DoX()
{
    if (pid() == 0)         // child process
        fs::remove(s_uds, s_errorCode);
}
