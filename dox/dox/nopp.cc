#include "dox.ih"

// static
void DoX::noPp(string const &line)
{
    size_t begin = 0;
    while (true)
    {
        size_t end = line.find("\\[", begin);   // ] (matching open bracket)
        if (end == string::npos)                // no pwd section(s) anymore
            break;
                                                // + 2 to show the bracket
        cout << line.substr(begin, end - begin + 2) << "...";
        begin = line.find("\\]", end);
    }
        
    cout << line.substr(begin) << '\n';
}
