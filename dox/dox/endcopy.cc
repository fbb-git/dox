#define XERR
#include "dox.ih"

void DoX::endCopy(UPtrEncrypt &newDb)
{
    d_decrypt.reset();
    newDb.reset();

    fs::rename(d_arg[0], d_arg[0] + ".bak"s, s_errorCode);
    fs::rename(d_newDbName, d_arg[0], s_errorCode);
}
