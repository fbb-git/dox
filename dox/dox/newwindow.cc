#define XERR
#include "dox.ih"

// static
string DoX::newWindow()
{
    cout << "Window name: ";
    string windowName;
    getline(cin, windowName);
    return String::trim(windowName);
}
