#define XERR
#include "dox.ih"

bool DoX::options()
{
    if (d_arg.nOptions() == 0 or 
        (d_arg.nOptions() == 1 and d_arg.option('i'))
    )
        return false;                       // no options

    for (char ch: string{ "adlLpt" })
    {
        if (not d_arg.option(ch))       // option --insert is handled by 
            continue;                   // 'startDaemon'

        switch (ch)
        {
            case 'a':
                addDb();                // add an entry to the db
            break;

            case 'd':
                defineDb();             // define a not-yet existing database
            break;

            case 'l':
            case 'L':
                listDb();               // list the db's content
            break;

            case 'p':
                chgPassphrase();        // change the passphrase
            break;

            case 't':
                daemonEnds();           // end the daemon
            break;
        }
        break;

    }
    return true;
}
