#ifndef INCLUDED_DOX_
#define INCLUDED_DOX_

#include <string>
#include <vector>
#include <memory>

#include <bobcat/signal>
#include <bobcat/fork>
#include <bobcat/isymcryptstream>
#include <bobcat/osymcryptstream>
#include <bobcat/localserversocket>

#include "../passphrase/passphrase.h"
#include "../doxtool/doxtool.h"

namespace FBB
{
    class Arg;
    class LocalClientSocket;
}

namespace std
{
    class error_code;
}


class DoX: public FBB::Fork, public FBB::SignalHandler
{
    using StringVect = std::vector<std::string>;
    using UPtrEncrypt = std::unique_ptr<FBB::OSymCryptStream<FBB::ENCRYPT>>;
    using UPtrDecrypt = std::unique_ptr<FBB::ISymCryptStream<FBB::DECRYPT>>;

    FBB::Arg const &d_arg;
    std::string d_newDbName;

    UPtrDecrypt d_decrypt;                          // opened by the daemon
    FBB::LocalServerSocket d_uds;       

    DoXTool d_xDoTool;
    Passphrase d_pp;

    static std::error_code s_errorCode;
    static char const s_uds[];
    static char const s_cryptIV[];

    public:
        DoX();
        ~DoX() override;
        int run();                              // 0: OK

    private:
        void childProcess() override;
        void parentProcess() override;

        void signalHandler(size_t signum) override;

        void verifyPassphrase();

        bool options();                     // handle the options

        void addDb();                       // add a new entry to the db
        void chgPassphrase();               // change the passphrase
        void defineDb();                    // define a not yet existing db
        void listDb();                      // -L: with passphrases
        void daemonEnds();                  // end the daemon

        int forward();                      // fwd cmd request to the daemon
        int startDaemon();

        void issue(std::ostream &out, std::string const &windowName, 
                                      std::string const &windowID);

        UPtrEncrypt copyDb(std::string const &windowName = "");
        void endCopy(UPtrEncrypt &newDb);

        void noDupCheck(std::string const &dbWindowName, 
                        std::string const &windowName) const;
        void dupCheck(std::string const &dbWindowName, 
                      std::string const &windowName) const;

        static int connect(FBB::LocalClientSocket &uds);    // returns the fd
        static int waitForReply(std::istream &in);

        static std::string newWindow();             // called from addDb
        static std::string newCmd();

        static void noUds(char const *context);
                                                    // Pp: passphrase
        static void noPp(std::string const &line);  // via ptr in listdb.cc
        static void withPp(std::string const &line);
};
        
#endif




