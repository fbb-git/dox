#define XERR
#include "dox.ih"

int DoX::run()
{
    if (options())                          // all options except for --insert
        return 0;

    if (d_arg.nArgs() == 1)                 // the database was specified:
        return startDaemon();               // start the daemon unless 1.uds
                                            // exists

                                            // determine
    d_xDoTool.targetWindow();               // the target window's details

    return forward();                       // forward the name + ID to
                                            // the daemon

//    // commands: use plain characters, \n for Enter \{msec\} for pause in msec
//    // \[...\] for passwords, not shown with -l, but shown with -L
//    // e.g.     crypt -c\n\{1000\}passphrasechars\n
//    // the command is converted to a vector of strings: 1st char c: remaining
//    // chars are command characters
//    // first char p: remaining chars define the msec pause
//
//    cout << "Command to issue? ";
//    string cmd;
//    getline(cin, cmd);
//    
//    d_xDoTool.issue(cmd);

    
}







