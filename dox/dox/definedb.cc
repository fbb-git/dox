#define XERR
#include "dox.ih"

// see also run.cc

// --define datafile /path/to/1.db
// 1.db may not exist, 

void DoX::defineDb()
{
    string rawData;
    d_arg.option(&rawData, 'd');

    noUds("remove it before defining the database");

    if (fs::exists(d_arg[0], s_errorCode))
        throw Exception{} << '`' << d_arg[0] << "' already exists";

    ofstream out = Exception::factory<ofstream>(d_arg[0]);
    OSymCryptStream<ENCRYPT> encrypt{ out, d_pp.cryptMethod(), d_pp.confirm(),
                                      s_cryptIV, 100 };

    ifstream data = Exception::factory<ifstream>(rawData);
    string line;
    size_t lineNr = 0;
    while (getline(data, line))
    {
        ++lineNr;
        string windowName = String::trim(line);
        if (windowName.empty())
            throw Exception{} << "No window name in line " << lineNr;

        if (not getline(data, line))
            throw Exception{} << "Specification missing at the end of " << 
                                 d_arg[0];
        ++lineNr;
        string spec = String::trim(line);
        if (spec.empty())
            throw Exception{} << "No specification for " << windowName <<
                                 " in line " << lineNr;

        encrypt << windowName << '\n' << spec << '\n';
    }
}




