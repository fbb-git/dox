#define XERR
#include "dox.ih"

void DoX::issue(ostream &out, string const &windowName, 
                              string const &windowID)
{
    string name(d_arg[0]);
    try
    {
        d_decrypt.reset(new ISymCryptStream<DECRYPT>{ 
                                              name, d_pp.cryptMethod(), 
                                              d_pp.passphrase(), 
                                              s_cryptIV, 100 });
    }
    catch(...)
    {
        out << "can't read the " << name << " db: ending the server" << endl;
        throw 0;
    }

    while (true)
    {
        string dbWindow;
        string dbCmd;

        if (not getline(*d_decrypt, dbWindow))
        {
            out << "no window `" << windowName << "' in the db" << endl;
            return;
        }

        if (not getline(*d_decrypt, dbCmd))
        {
            out << "no command found for window `" << windowName << 
                                                "' in the db" << endl;
            return;
        }

        if (dbWindow == windowName)
        {
            out << "OK" << endl;

            d_xDoTool.issue(windowID, dbCmd);
            return;
        }
    }
}

