#define XERR
#include "dox.ih"

// static
int DoX::connect(LocalClientSocket &uds)
{
    try
    {
        return uds.connect();
    }
    catch (...)
    {
        throw Exception{} << "can't connect to the daemon: is it active?";
    }
    
}
