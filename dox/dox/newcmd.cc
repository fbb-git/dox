#define XERR
#include "dox.ih"

// static
string DoX::newCmd()
{
    Tty tty;   
    cout << "Show the entered command? [yN] ";
    string type;
    getline(cin, type);
    bool off = type.empty() or type.front() != 'y';
    if (off)
        tty.echo(Tty::OFF);

    cout << "Command to issue: ";
    string cmd;
    getline(cin, cmd);
    if (off)
        cout << '\n';

    return String::trim(cmd);
}
