#define XERR
#include "dox.ih"

void DoX::verifyPassphrase()
{
    if (d_arg.nArgs() == 0)
        throw Exception{} << "path to dox.db must be specified";

    string name{ d_arg[0] };
    size_t attempt = 0;

    while (true)
    {
        try
        {           
                // must use a name, or the ifstream is deleted
            d_decrypt.reset(
                new ISymCryptStream<DECRYPT>{ name, d_pp.cryptMethod(), 
                                              d_pp.request(), 
                                              s_cryptIV, 100 });
            return;
        }
        catch (...)
        {
            if (++attempt == 3)
                throw Exception{} << 
                    "Three incorrect passphrase specifications: terminating";

            cerr << "Invalid passphrase, try again\n";
        }
    }
}




