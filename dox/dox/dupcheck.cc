#define XERR
#include "dox.ih"

void DoX::dupCheck(string const &dbWindowName, string const &windowName) const
{
    if (dbWindowName == windowName)
    {
        fs::remove(d_newDbName, s_errorCode);   // remove the new file

        throw Exception{} << "Window " << windowName <<
                             " already defined in " << d_arg[0];
    }
}
