#define XERR
#include "dox.ih"

void DoX::listDb()
{
    verifyPassphrase();                 // opens d_decrypt

    void (*listPtr)(string const &) = d_arg.option('L') ? withPp : noPp;

    string line;
    while (getline(*d_decrypt, line))
        (*listPtr)(line);
}
