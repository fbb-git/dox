#define XERR
#include "dox.ih"

void DoX::chgPassphrase()
{
    noUds("end the daemon before changing the passphrase");

    verifyPassphrase();                 // opens d_decrypt

    d_pp.confirm("new ");

    UPtrEncrypt encrypt = copyDb();
    endCopy(encrypt);
}
