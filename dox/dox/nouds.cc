#include "dox.ih"

//static
void DoX::noUds(char const *context)
{
    if (fs::exists(s_uds, s_errorCode))
        throw Exception{} << '`' << s_uds << 
                            "' exists: " << context;
}
