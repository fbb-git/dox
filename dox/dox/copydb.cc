#define XERR
#include "dox.ih"

DoX::UPtrEncrypt DoX::copyDb(string const &windowName)
{
    string arg0 = d_arg[0];
    d_newDbName =  arg0 + ".new";

    UPtrEncrypt encrypt{ 
                    new OSymCryptStream<ENCRYPT>{
                            d_newDbName, d_pp.cryptMethod(), 
                            d_pp.passphrase(), s_cryptIV, 100 }
                };

    void (DoX::*checkDup)(string const &, string const &) const =
            windowName.empty() ?
                    &DoX::noDupCheck
            :
                    &DoX::dupCheck;
        
    string dbWindowName;
    while (getline(*d_decrypt, dbWindowName))
    {
        (this->*checkDup)(dbWindowName, windowName);

        string spec;
        getline(*d_decrypt, spec);

        *encrypt << dbWindowName << '\n' << spec << '\n';
    }

    return encrypt;
}
