#define XERR
#include "dox.ih"

void DoX::signalHandler(size_t signal)
{
    if (signal == SIGTERM)
        throw 0;
}
