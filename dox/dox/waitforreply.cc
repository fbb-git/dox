#define XERR
#include "dox.ih"

// static
int DoX::waitForReply(istream &in)
{
    string reply;
    if (not getline(in, reply))
        throw Exception{} << "no reply from the daemon...";

    if (reply.find("OK") != 0)
    {
        cerr << reply << '\n';
        return 1;
    }

    if (reply.length() > 2)
        cout << reply << '\n';

    return 0;
}
