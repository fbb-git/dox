#define XERR
#include "dox.ih"

int DoX::startDaemon()
{
                                    // there's no data base
    if (not fs::exists(d_arg[0], s_errorCode))
        throw Exception{} << "can't read `" << d_arg[0] << '\'';

    noUds("daemon already running?");   // s_uds may not yet exist

    verifyPassphrase();             // get the passwd, verify it's correct

    if (not d_arg.nOptions() or d_arg.option('i'))
        fork();

    return 0;                        // all done wrt activating the daemon
}
