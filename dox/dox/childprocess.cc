#define XERR
#include "dox.ih"


// overrides
void DoX::childProcess()
{
    d_uds.open(s_uds);          // open the unix domain socket or exception

    prepareDaemon();

    d_uds.listen();

    Signal::instance().add(SIGTERM, *this);

    while (true)
    {
        int fd = d_uds.accept();

        OFdStream out(fd);          // stream to write to client
        IFdStream in(fd);           // stream to read from client

        string windowName;
        string windowID;
        while (getline(in, windowName))
        {
            if (windowName == "quit")
            {
                out << "OK: the daemon ended" << endl;
                return;
            }
            getline(in, windowID);
                                    // issue the request for 'windowName'
            issue(out, windowName, windowID);
        }
    }
}





