#include "dox.ih"

// overrides
void DoX::parentProcess()
{
    cout << "daemon active\n";

    if (d_arg.option('i'))
    {
        d_xDoTool.useCurrentWindow();
        cout << "issuing the command associated with " << 
                    d_xDoTool.windowName() << "..." << endl;
        forward();        
    }

    throw 0;
}
