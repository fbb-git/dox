#ifndef INCLUDED_DOXTOOL_
#define INCLUDED_DOXTOOL_

#include <string>
#include <vector>

#include <bobcat/coutextractor>

class DoXTool
{
    using StringVect = std::vector<std::string>;

    FBB::CoutExtractor d_extractor;

    std::string d_windowName;
    std::string d_windowID;

    static std::string s_keyCmd;
    static std::string s_typeCmd;

    public:
        void useCurrentWindow();
        void targetWindow();
        void issue(std::string const &windowID, std::string const &cmd);
        std::string const &windowName() const;              // inline
        std::string const &windowID() const;                // inline

    private:
        void getWindowName();

        static StringVect parse(std::string const &cmd);
        static std::string rmBrackets(std::string const &cmd);
};
        
inline std::string const &DoXTool::windowName() const
{
    return d_windowName;
}

inline std::string const &DoXTool::windowID() const
{
    return d_windowID;
}

#endif


