#define XERR
#include "doxtool.ih"

void DoXTool::useCurrentWindow()
{
    d_extractor.execute("/usr/bin/xdotool getactivewindow");
    getline(d_extractor, d_windowID);

    getWindowName();
}
