#define XERR
#include "doxtool.ih"

void DoXTool::targetWindow()
{
    cout << "L-click the mouse in the window to use" << endl;

    d_extractor.execute("/usr/bin/xdotool selectwindow");

    getline(d_extractor, d_windowID);

    getWindowName();
}
