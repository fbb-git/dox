#define XERR
#include "doxtool.ih"

// static
string DoXTool::rmBrackets(string const &cmd)
{
    string ret;

    size_t begin = 1;
    while (true)
    {
        size_t end = cmd.find("\\[", begin);        // find the open bracket
        size_t end2 = cmd.find("\\]", begin);       // find the close bracket
        if (end2 < end)
            end = end2;
        
        if (end == string::npos)                    // done if no more
            break;

        ret += cmd.substr(begin, end - begin);      // cp until the bracket
        begin = end + 2;                            // skip the bracket
    }
    ret += cmd.substr(begin);

    return ret;
}

