#include "doxtool.ih"

// static
DoXTool::StringVect DoXTool::parse(string const &cmd)
{
    StringVect ret;
    string part = "t";
    for (auto begin = cmd.begin(), end = cmd.end(); begin != end; )
    {
        if (*begin != '\\')             // normal character: add to part
        {
//            part += s_Xsymbols[*begin++];
            part += *begin++;
            continue;
        }

        switch (*++begin)               // skip the \, interpret the next
        {
            case 'n':
                if (not part.empty())
                    ret.push_back(part);
                ret.push_back("kKP_Enter");
                part = "t";
                ++begin;
            continue;

            case '{':
            {
                part.front() = 'd';
                size_t curlyIdx = begin - cmd.begin();
                size_t endCurly = cmd.find('}') - curlyIdx;
                part += cmd.substr(curlyIdx + 1, endCurly - 2);
                ret.push_back(part);
                part = "t";
                begin += endCurly + 1;
            }
            break;

            case '[':
            case ']':
                ++begin;
            continue;
        }
    }

    if (part.length() > 1)
        ret.push_back(part);

    return ret;

}               

