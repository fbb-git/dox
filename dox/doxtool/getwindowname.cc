#define XERR
#include "doxtool.ih"

void DoXTool::getWindowName()
{
    d_extractor.execute("/usr/bin/xdotool getwindowname " + d_windowID);
    getline(d_extractor, d_windowName);
}
