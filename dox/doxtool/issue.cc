#define XERR
#include "doxtool.ih"

void DoXTool::issue(string const &id, string const &cmdSpec)
{
    string cmd = String::trim(cmdSpec);
    string windowID = id;

    StringVect parts = parse(cmd);

    Proc xDoTool{ "", Proc::IGNORE_COUT };

    Selector selector;

    if (windowID.front() == '-')            // cmd for the window running dox
    {
        windowID.erase(0, 1);               // rm the initial '-'

        selector.setAlarm(0, 750'000);      // wait 3/4 second
        selector.wait();
    }
        
    for (string const &cmd: parts)
    {
        switch (cmd.front())
        {
            case 'd':
            {
                size_t sleepTime = stoul(cmd.substr(1));
//                xerr("sleepTime: " << sleepTime);
                selector.setAlarm(sleepTime / 1000, sleepTime % 1000 * 1000);
                selector.wait();
            }
            break;

            case 'k':
//                xerr(s_keyCmd << windowID << ' ' << cmd.substr(1));
                xDoTool = s_keyCmd + windowID + ' ' + cmd.substr(1);
                xDoTool.finish();
            break;

            case 't':
//                xerr(s_typeCmd << windowID << " '" << cmd.substr(1) << '\'');
                xDoTool = s_typeCmd + windowID + " '" + cmd.substr(1) + '\'';
                xDoTool.finish();
            break;
        }
    }
}




